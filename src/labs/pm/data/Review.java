/*
 * Copyright (c) 2021. Free Software Foundation, Inc. <www.djm.eu5.org>
 *  Everyone is permitted to copy and distribute verbatim copies
 *  of this license document, but changing it is not allowed.
 */

package labs.pm.data;import java.io.Serializable;

public class Review implements Comparable<Review>, Serializable {
    private final String comments;
    private final Rating rating;

    public Review(String comments, Rating rating) {
        this.comments = comments;
        this.rating = rating;
    }

    public String getComments() {
        return comments;
    }

    public Rating getRating() {
        return rating;
    }

    @Override
    public String toString() {
        return "Review{" + "comments='" + comments + '\'' + ", rating=" + rating + '}';
    }

    @Override
    public int compareTo(Review other) {
        //best use parameter object - current object for big to small
        return other.rating.ordinal()-this.rating.ordinal();
    }
}
