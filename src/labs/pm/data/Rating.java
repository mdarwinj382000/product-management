/*
 * Copyright (c) 2021. Free Software Foundation, Inc. <www.djm.eu5.org>
 *  Everyone is permitted to copy and distribute verbatim copies
 *  of this license document, but changing it is not allowed.
 */

package labs.pm.data;/**
 * The enum Collection of different ratings
 */
// enum is public static final so the content cannot be changed at run time
public enum Rating {
    //everytime the enum keyword get called the contructor also gets called
    //BlackStar->\u2605   Whitestar->\u2606
    NOT_RATED("\u2606\u2606\u2606\u2606\u2606"),
    ONE_STAR("\u2605\u2606\u2606\u2606\u2606"),
    TWO_STAR("\u2605\u2605\u2606\u2606\u2606"),
    THREE_STAR("\u2605\u2605\u2605\u2606\u2606"),
    FOUR_STAR("\u2605\u2605\u2605\u2605\u2606"),
    FIVE_STAR("\u2605\u2605\u2605\u2605\u2605");
    private final String stars;
    //constructor can be default or private
    Rating(String stars) {
        this.stars = stars;
    }

    //can have ay no of public methods no other access modifiers
    public String getStars() {
        return stars;
    }
}
