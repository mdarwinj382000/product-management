/*
 * Copyright (c) 2021. Free Software Foundation, Inc. <www.djm.eu5.org>
 *  Everyone is permitted to copy and distribute verbatim copies
 *  of this license document, but changing it is not allowed.
 */

package labs.pm.data;import java.io.*;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.FormatStyle;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

// this is immutable class but didnt make the variables final because of intialization later.
// and the feature in cannot change the value but can get the value using getters(accessors)
//there is no mutator in this class except inti
public class ProductManager {
    private Map<Product, List<Review>> products = new HashMap<>();
    //private ResourceFormatter formatter;
    private final ResourceBundle config = ResourceBundle.getBundle("labs.pm.data.config");
    private final MessageFormat reviewFormat = new MessageFormat(config.getString("review.data.format"));
    private final MessageFormat productFormat = new MessageFormat(config.getString("product.data.format"));
    private final Path reportsFolder = Path.of(config.getString("reports.folder"));
    private final Path dataFolder = Path.of(config.getString("data.folder"));
    private final Path tempFolder = Path.of(config.getString("temp.folder"));
    private static final Map<String, ResourceFormatter> formatters = Map.of("en-GB",
                                                                            new ResourceFormatter(Locale.UK),
                                                                            "en-US",
                                                                            new ResourceFormatter(Locale.US),
                                                                            "fr-FR",
                                                                            new ResourceFormatter(Locale.FRANCE),
                                                                            "ru-RU",
                                                                            new ResourceFormatter(new Locale("ru",
                                                                                                             "RU")),
                                                                            "zh-CN",
                                                                            new ResourceFormatter(Locale.CHINA));
    private static final Logger LOGGER = Logger.getLogger(ProductManager.class.getName());
    private static final ProductManager pm = new ProductManager();
    private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    private final Lock writeLock = lock.writeLock();
    private final Lock readLock = lock.readLock();


    public static ProductManager getInstance() {
        return pm;
    }

//    public ProductManager(Locale locale) {
//        this(locale.toLanguageTag());
//
//    }

    private ProductManager() {
//        changeLocale(LanguageTag);
        loadAllData();
    }

//    public void changeLocale(String languageTag) {
//        formatter = formatters.getOrDefault(languageTag, formatters.get("en-GB"));
//    }

    public static Set<String> getSupportedLocales() {
        return formatters.keySet();
    }

    public Product createProduct(int id, String name, BigDecimal price, Rating rating, LocalDate bestBefore) {
        Product product = null;
        try {
            writeLock.lock();
            product = new Food(id, name, price, rating, bestBefore);
            products.putIfAbsent(product, new ArrayList<>());
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "Error adding product " + e.getMessage());
        } finally {
            writeLock.unlock();
        }
        return product;
    }

    public Product createProduct(int id, String name, BigDecimal price, Rating rating) {
        Product product = null;
        try {
            writeLock.lock();
            product = new Drink(id, name, price, rating);
            products.putIfAbsent(product, new ArrayList<>());
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "Error adding product " + e.getMessage());
        } finally {
            writeLock.unlock();
        }
        return product;
    }

    public Product reviewProduct(int id, Rating rating, String comments) {
        try {
            writeLock.lock();
            return reviewProduct(findProduct(id), rating, comments);
        } catch (ProductManagerException e) {
            //e.printStackTrace();
            LOGGER.log(Level.INFO, e.getMessage());
            return null;
        } finally {
            writeLock.unlock();
        }
    }

    private Product reviewProduct(Product product, Rating rating, String comments) {
        List<Review> reviews = products.get(product);
        products.remove(product, reviews);
        reviews.add(new Review(comments, rating));
//        int sum = 0;
//        for (Review review : reviews) {
//            sum += review.getRating()
//                         .ordinal();
//        }
//        int sum = 0, i = 0;
//        boolean reviewed = false;
//        while (i < reviews.length && !reviewed) {
//            if (reviews[i] == null) {
//                reviews[i] = new Review(comments, rating);
//                reviewed = true;
//            }
//            sum += reviews[i].getRating()
//                             .ordinal();
//            i++;
//        }
//        review = new Review(comments, rating);
//        product = product.applyRating(Rateable.convert(Math.round((float) sum / reviews.size())));
        product = product.applyRating(Rateable.convert((int) Math.round(reviews.stream()
                                                                               .mapToInt(r -> r.getRating()
                                                                                               .ordinal())
                                                                               .average()
                                                                               .orElse(0))));
        products.put(product, reviews);
        return product;
    }

    public Product findProduct(int id) throws ProductManagerException {
//        Product result = null;
//        for (Product product : products.keySet()) {
//            if (product.getId() == id) {
//                result = product;
//                break;
//            }
//        }
//        return result;
        try {
            writeLock.lock();
            return products.keySet()
                           .stream()
                           .filter(product -> product.getId() == id)
                           .findFirst()
                           .orElseThrow(() -> new ProductManagerException("Product with id" + id + "not found"));//.get();//.orElse(null);//(or) .orElseGet(()->null)
        } finally {
            writeLock.unlock();
        }
    }

    public synchronized void printProductReport(int id, String languageTag, String client) {
        try {
            // i didn't use readLock due to it thread locking the made the program infinite
            //instead of using readLock used synchronized
            // readLock.lock();
            printProductReport(findProduct(id), languageTag, client);
        } catch (ProductManagerException e) {
            // e.printStackTrace();
            LOGGER.log(Level.INFO, e.getMessage());
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Error printing product report" + e.getMessage(), e);
        } finally {
            // readLock.unlock();
        }
    }

    //reports the print in reportfile of a product
    private synchronized void printProductReport(Product product, String languageTag, String client) throws IOException {
        ResourceFormatter formatter = formatters.getOrDefault(languageTag, formatters.get("en-Gb"));
        List<Review> reviews = products.get(product);
        //  StringBuilder txt = new StringBuilder();
        Collections.sort(reviews);
        Path productFile = reportsFolder.resolve(MessageFormat.format(config.getString("report.file"),
                                                                      product.getId(),
                                                                      client));
        try (PrintWriter out = new PrintWriter(new OutputStreamWriter(Files.newOutputStream(productFile,
                                                                                            StandardOpenOption.CREATE),
                                                                      StandardCharsets.UTF_8))) {
            //as we are playing with path each os has their own new line character format
            // so System class takes care of newline as make it platform independent code.
            out.append(formatter.formatProduct(product))
               .append(System.lineSeparator());

//        for (Review review : reviews) {
//            txt.append(formatter.formatReview(review));
//            txt.append('\n');
//        }
            if (reviews.isEmpty()) {
                out.append(formatter.getText())
                   .append(System.lineSeparator());
            } else {
                out.append(reviews.stream()
                                  .map(r -> formatter.formatReview(r) + System.lineSeparator())
                                  .collect(Collectors.joining()));
            }
        }
    }

    public void printProducts(Predicate<Product> filter, Comparator<Product> sorter, String languageTag) {
        try {
            readLock.lock();
            ResourceFormatter formatter = formatters.getOrDefault(languageTag, formatters.get("en-GB"));
//        List<Product> productList = new ArrayList<>(products.keySet());
//        productList.sort(sorter);
            StringBuilder txt = new StringBuilder();
//        for (Product product : productList) {
//            txt.append(formatter.formatProduct(product));
//            txt.append("\n");
//        }
            products.keySet()
                    .stream()
                    .sorted(sorter)
                    .filter(filter)
                    .forEach(p -> txt.append(formatter.formatProduct(p))
                                     .append("\n"));
            System.out.println(txt);
        } finally {
            readLock.unlock();
        }
    }

    public void dumpData() {
        try {
            if (Files.notExists(tempFolder)) {
                Files.createDirectory(tempFolder);
            }
            Path tempFile = tempFolder.resolve(MessageFormat.format(config.getString("temp.file"),
                                                                    LocalDateTime.now()/*normal datetimeparsing the char<:> is not supported in file creation*/
                                                                                 .toString()
                                                                                 .replaceAll(":", "-")));
            try (ObjectOutputStream out = new ObjectOutputStream(Files.newOutputStream(tempFile,
                                                                                       StandardOpenOption.CREATE))) {
                out.writeObject(products);
//                products = new HashMap<>();
            }
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Error dumping data " + e.getMessage(), e);
        }
    }

    @SuppressWarnings("unchecked")
    public void restoreData() {
        try {
            Path tempFile = Files.list(tempFolder)
                                 .filter(path -> path.getFileName()
                                                     .toString()
                                                     .endsWith("tmp"))
                                 .findFirst()
                                 .orElseThrow();
            try (ObjectInputStream in = new ObjectInputStream(Files.newInputStream(tempFile,
                                                                                   StandardOpenOption.DELETE_ON_CLOSE /*or use StandardOpenOption.READ*/))) {
                //Warning occurs:
                // due to compiler does not guarantee that the type of deserialized data is of sametype you serialized
                // or specific type asked and deserialized data is the same type or not
                products = (HashMap) in.readObject();
            }
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error restoring data " + e.getMessage(), e);
        }
    }

    private void loadAllData() {
        try {
            products = Files.list(dataFolder)
                            .filter(file -> file.getFileName()
                                                .toString()
                                                .startsWith("product"))
                            .map(this::loadProduct)
                            .filter(Objects::nonNull)
                            .collect(Collectors.toMap(product -> product, this::loadReviews));
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Error loading data " + e.getMessage());
        }
    }

    private List<Review> loadReviews(Product product) {
        List<Review> reviews = null;
        Path file = dataFolder.resolve(MessageFormat.format(config.getString("reviews.data.file"), product.getId()));
        if (Files.notExists(file)) {
            reviews = new ArrayList<>();
        } else {
            try {
                reviews = Files.lines(file, StandardCharsets.UTF_8)
                               .map(this::parseReview)
                               .filter(Objects::nonNull)
                               .collect(Collectors.toList());
            } catch (IOException e) {
                LOGGER.log(Level.WARNING, "Error loading review " + e.getMessage());
            }
        }
        return reviews;
    }

    private Product loadProduct(Path file) {
        Product product = null;
        try {
            product = parseProduct(Files.lines(dataFolder.resolve(file), StandardCharsets.UTF_8)
                                        .findFirst()
                                        .orElseThrow());
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Error loading product " + e.getMessage());
        }
        return product;
    }

    private Review parseReview(String text) {
        Review review = null;
        try {
            Object[] values = reviewFormat.parse(text);
            review = new Review((String) values[1], Rateable.convert(Integer.parseInt((String) values[0])));
        } catch (ParseException | NumberFormatException e) {
            //e.printStackTrace();
            LOGGER.log(Level.WARNING, "Error parsing review " + text, e.getMessage());
            //simple e gives brief exception message
            //while e.getMessage() gives short exception message
        }
        return review;
    }

    private Product parseProduct(String text) {
        Product product = null;
        try {
            Object[] values = productFormat.parse(text);
            int id = Integer.parseInt((String) values[1]);
            String name = (String) values[2];
            BigDecimal price = BigDecimal.valueOf(Double.parseDouble((String) values[3]));
            Rating rating = Rateable.convert(Integer.parseInt((String) values[4]));
            switch ((String) values[0]) {
                case "D":
                    // createProduct(id, name, price, rating);
                    product = new Drink(id, name, price, rating);
                    break;
                case "F":
                    //date is passed in format y-m-d
                    LocalDate bestbefore = LocalDate.parse((String) values[5]);
                    // createProduct(id, name, price, rating, bestbefore);
                    product = new Food(id, name, price, rating, bestbefore);
                    break;
            }
        } catch (ParseException | NumberFormatException | DateTimeParseException e) {
            //e.printStackTrace();
            LOGGER.log(Level.WARNING, "Error parsing review " + text, e.getMessage());
            //simple e gives brief exception message
            //while e.getMessage() gives short exception message
        }
        return product;
    }

    public Map<String, String> getDiscounts(String languageTag) {
        try {
            readLock.lock();
            ResourceFormatter formatter = formatters.getOrDefault(languageTag, formatters.get("en-Gb"));
            return products.keySet()
                           .stream()
                           .collect(Collectors.groupingBy(p -> p.getRating()
                                                                .getStars(),
                                                          Collectors.collectingAndThen(Collectors.summingDouble(p -> p.getDiscount()
                                                                                                                      .doubleValue()),
                                                                                       formatter.moneyFormat::format)));
        } finally {
            readLock.unlock();
        }
    }

    static class ResourceFormatter {
        private final ResourceBundle resources;
        private final DateTimeFormatter dateFormat;
        private final NumberFormat moneyFormat;

        private ResourceFormatter(Locale locale) {
            resources = ResourceBundle.getBundle("labs.pm.data.resources", locale);
            dateFormat = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)
                                          .localizedBy(locale);
            moneyFormat = NumberFormat.getCurrencyInstance(locale);
        }

        private String formatProduct(Product product) {
            return MessageFormat.format(resources.getString("product"),
                                        product.getName(),
                                        moneyFormat.format(product.getPrice()),
                                        product.getRating()
                                               .getStars(),
                                        dateFormat.format(product.getBestBefore()));
        }

        private String formatReview(Review review) {
            return MessageFormat.format(resources.getString("review"),
                                        review.getRating()
                                              .getStars(),
                                        review.getComments());
        }

        private String getText() {
            return resources.getString("no.reviews");
        }
    }
}
