/*
 * Copyright (c) 2021. Free Software Foundation, Inc. <www.djm.eu5.org>
 *  Everyone is permitted to copy and distribute verbatim copies
 *  of this license document, but changing it is not allowed.
 */

package labs.pm.data;//can make this annotation when there is only one abstract method
// interface can have only default ,static and private methods implementations
// this interface is generic interface so need to mention what type of interface<Type> when used
@FunctionalInterface
public interface Rateable<T> {
    //can have only constant variables
    //default variables are constants i.e., public static final
    Rating DEFAULT_RATING = Rating.NOT_RATED;

    //default abstract methods are public abstract
    T applyRating(Rating rating);

    // default methods becomes abstract when have a conflict during multiple inheritance
    //i.e., when a class implements two interface then both have same defined default interfaces
    //then the compiler makes the method default to abstract
    default T applyRating(int stars) {
        return applyRating(convert(stars));
    }

    //implcitly all methods are public
    default Rating getRating() {
        return DEFAULT_RATING;
    }

    //can be called directly using interface Name eg.,Rateable.convert(int) since it is static
    //satisfies the static rule
    static Rating convert(int stars) {
        return (stars >= 0 && stars <= 5) ? Rating.values()[stars] : DEFAULT_RATING;
    }
}
