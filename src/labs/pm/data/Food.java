/*
 * Copyright (c) 2021. Free Software Foundation, Inc. <www.djm.eu5.org>
 *  Everyone is permitted to copy and distribute verbatim copies
 *  of this license document, but changing it is not allowed.
 */

package labs.pm.data;import java.math.BigDecimal;
import java.time.LocalDate;

public final class Food extends Product {
    private final LocalDate bestBefore;

    Food(int id, String name, BigDecimal price, Rating rating, LocalDate bestBefore) {
        super(id, name, price, rating);
        this.bestBefore = bestBefore;
    }

    @Override
    public String toString() {
        return super.toString() + " " + bestBefore;
    }

    @Override
    public BigDecimal getDiscount() {
        return (bestBefore.isEqual(LocalDate.now())) ? super.getDiscount() : BigDecimal.ZERO;
    }

    /**
     * Get the Value of best before date for the product
     *
     * @return the value of bestbefore
     */

    public LocalDate getBestBefore() {
        return bestBefore;
    }


    @Override
    public Product applyRating(Rating rating) {
        return new Food(getId(), getName(), getPrice(), rating, bestBefore);
    }
}
