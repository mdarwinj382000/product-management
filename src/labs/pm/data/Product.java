/*
 * Copyright (c) 2021. Free Software Foundation, Inc. <www.djm.eu5.org>
 *  Everyone is permitted to copy and distribute verbatim copies
 *  of this license document, but changing it is not allowed.
 */

package labs.pm.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

import static java.math.RoundingMode.HALF_UP;

/**
 * {@code Product} class thinks of buying product documentation
 * This class is a immutable class
 *
 * @author mdarw
 * @version 1.0
 */
public abstract class Product implements Rateable<Product>, Serializable {
    /**
     * a constant that defines {@link java.math.BigDecimal BigDecimal} the value of discount rate.
     * <br>
     * DiscountRate is 10%
     */
    public static final BigDecimal DISCOUNT_RATE = BigDecimal.valueOf(0.1);
    private final int id;
    private final String name;
    private final BigDecimal price;
    private final Rating rating;

//    Product() {
//        this(0, "no name", BigDecimal.ZERO);
//    }

//    Product(int id, String name, BigDecimal price) {
//        this(id, name, price, NOT_RATED);
//    }

    Product(int id, String name, BigDecimal price, Rating rating)  {
        this.id = id;
        this.name = name;
        this.price = price;
        this.rating = rating;
    }

    public LocalDate getBestBefore() {
        return LocalDate.now();
    }

    @Override
    public String toString() {
        return id + " " + name + " " + price + " " + getDiscount() + " " + rating.getStars() + " " + getBestBefore();
    }

    // checks whether the object is subclass instance or not
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        Product product = (Product) o;
//        return id == product.id && Objects.equals(name, product.name);
//    }

    //checks whether the object is instance of parent class or not
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return id == product.id; //&& Objects.equals(name, product.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public int getId() {
        return id;
    }

//    /**
//     * super setting values.
//     * <br>
//     * method documentation.
//     *
//     * @param id pass the id value
//     */
//    public void setId(final int id) {
//        this.id = id;
//    }

    public String getName() {
        return name;
    }

//    public void setName(final String name) {
//        this.name = name;
//    }

    public BigDecimal getPrice() {
        return price;
    }

//    public void setPrice(final BigDecimal price) {
//        this.price = price;
//    }

    /**
     * Calculates discount based on the product price and
     * {@link DISCOUNT_RATE discount rate}
     *
     * @return a {@link java.math.BigDecimal BigDecimal} value of the discount
     */
    public BigDecimal getDiscount() {
        return price.multiply(DISCOUNT_RATE.setScale(2, HALF_UP));
    }

    @Override
    public Rating getRating() {
        return rating;
    }

//    public abstract Product applyRating(Rating newrating);
}
