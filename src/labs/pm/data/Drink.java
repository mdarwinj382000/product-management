/*
 * Copyright (c) 2021. Free Software Foundation, Inc. <www.djm.eu5.org>
 *  Everyone is permitted to copy and distribute verbatim copies
 *  of this license document, but changing it is not allowed.
 */

package labs.pm.data;import java.math.BigDecimal;
import java.time.LocalTime;

public final class Drink extends Product {
    Drink(int id, String name, BigDecimal price, Rating rating) {
        super(id, name, price, rating);
    }

    @Override
    public BigDecimal getDiscount() {
        LocalTime now = LocalTime.now();
        return (now.isAfter(LocalTime.of(17  , 30)) && now.isBefore(LocalTime.of(18, 30))) ? super.getDiscount()
                                                                                         : BigDecimal.ZERO;
    }


    @Override
    public Product applyRating(Rating rating) {
        return new Drink(getId(), getName(), getPrice(), rating);
    }
}
