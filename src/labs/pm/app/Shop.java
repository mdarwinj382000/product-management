
/*
 * Copyright (c) 2021. Free Software Foundation, Inc. <www.djm.eu5.org>
 *  Everyone is permitted to copy and distribute verbatim copies
 *  of this license document, but changing it is not allowed.
 */

package labs.pm.app;

import labs.pm.data.Product;
import labs.pm.data.ProductManager;
import moduler.Helper;

import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level; 
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static labs.pm.data.Rating.FOUR_STAR;

//makes the member static since it is static then easy to use directly without mentioning
// as if the member belongs this to this class and defined inside this class

// all the classes are mostly and must be public
// then classes of same file cannot be public other the than main class
// that to default only
public class Shop {

    /**
     * arguments are passed at compile time
     *
     * @param args passing arguments as many you want
     */
    public static void main(String[] args) {
        ProductManager pm = ProductManager.getInstance();
        AtomicInteger clientCount = new AtomicInteger(0);
        Callable<String> client = () -> {
            String clientId = "Client" + clientCount.incrementAndGet();
            String threadName = Thread.currentThread()
                                      .getName();
            Helper.printer(threadName);
            int productId = ThreadLocalRandom.current()
                                             .nextInt(3) + 101;
            String languageTag = ProductManager.getSupportedLocales()
                                               .stream()
                                               .skip(ThreadLocalRandom.current()
                                                                      .nextInt(4))
                                               .findFirst()
                                               .get();
            StringBuilder log = new StringBuilder();
            log.append(clientId)
               .append(" ")
               .append(threadName)
               .append("\n-\tstart of log\t-\n");
            log.append(pm.getDiscounts(languageTag)
                         .entrySet()
                         .stream()
                         .map(entry -> entry.getKey() + "\t" + entry.getValue())
                         .collect(Collectors.joining("\n")));
            Product product = pm.reviewProduct(productId, FOUR_STAR, "Yet another review");
            log.append(product != null ? "\nProduct " + productId + " review\n"
                                       : "\nProduct " + productId + " not reviewed\n");
            pm.printProductReport(productId, languageTag, clientId);
            log.append(clientId)
               .append(" generated report for ")
               .append(productId)
               .append(" product");
            log.append("\n-\tend of log\t-\n");
            return log.toString();
        };
        List<Callable<String>> clients = Stream.generate(() -> client)
                                               .limit(5)
                                               .collect(Collectors.toList());
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        try {
            List<Future<String>> results = executorService.invokeAll(clients);
            executorService.shutdown();
            results.forEach(result -> {
                try {
                    System.out.println(result.get());
                } catch (InterruptedException | ExecutionException e) {
                    Logger.getLogger(Shop.class.getName())
                          .log(Level.SEVERE, null, e);
                }
            });
        } catch (InterruptedException e) {
            Logger.getLogger(Shop.class.getName())
                  .log(Level.SEVERE, "Error invoking clients", e);
        }
        pm.dumpData();
        pm.restoreData();
//        Comparator<Product> ratingSorter = (p1, p2) -> p2.getRating()
//                                                         .ordinal() - p1.getRating()
//                                                                        .ordinal();
//        pm.printProducts(p -> p.getPrice()
//                               .floatValue() < 3, ratingSorter,"en-GB");
//        pm.getDiscounts()
//          .forEach((rating, discount) -> System.out.println(rating + "\t" + discount));
//        Comparator<Product> priceSorter = (p1, p2) -> p2.getPrice()
//                                                        .compareTo(p1.getPrice());
//        pm.printProducts(priceSorter);
//        pm.printProducts(ratingSorter.thenComparing(priceSorter));
//        pm.printProducts(ratingSorter.thenComparing(priceSorter).reversed());


    }
}
