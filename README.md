# PRODUCT MANAGEMENT :thumbsup: :v: :monkey:
   - samplemoduleprinter
   - productmanagement

```mermaid
graph TD;
   ProductManagement[HEADERPACKAGE]-->samplemoduleprinter;
   ProductManagement[HEADERPACKAGE]-->productmanagement;
```

##### The projects consists of Two modules :star2:
>>>
{+samplemoduleprinter+}-->consists of only single package  
~~~
It's just printing the number of threads and their names started during runtime.
~~~
{+productmanagement+}-->consists of three packages
>>>

### STRUCTURE: :family:
   - {-src.labs.pm.app-} => has the main file shop.java
   - {-src.labs.pm.data-} => has the product managing classes
   - {-src.labs.data-} => has the sample products and reviews file
   - {-src.labs.report-} => has the files when the program is executed i.e., combined form of products and reviews creation of reports files for each single product.
   - {-samplemoduleprinter.src-} => has the helper class

### NOTE: :speak_no_evil: 
   - have a method in product class to insert the data in strings.. through cmd
   - can insert data by creating text files ...
   - can {+REFER DOCS+} FOR FOR PERFECT STRUCTURE OF CLASSES
